#!/usr/bin/env bash

tag='spectata-execution'
sudo docker build --build-arg http_proxy=$http_proxy --build-arg https_proxy=$https_proxy --build-arg no_proxy=$no_proxy -t=$tag .

echo "Built: $tag"
