FROM base-java
MAINTAINER maartenduijn@msn.com

# ------------------------------------------------------------------------------------
# Maven
# ------------------------------------------------------------------------------------
ENV M2_HOME=/opt/maven \
    MAVEN_VERSION=3.3.9
ENV PATH=${PATH}:$M2_HOME/bin

RUN wget http://apache.cs.uu.nl/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz -O /tmp/maven.tgz \
    && tar zxf /tmp/maven.tgz --directory=/opt \
    && ln -s `find /opt -name 'apache-maven-*'` /opt/maven \
    && rm -rf /tmp/*
COPY configs/settings.xml /opt/maven/conf/settings.xml
RUN chmod +r /opt/maven/conf/settings.xml

# ------------------------------------------------------------------------------------
# Git 2.7.0
# ------------------------------------------------------------------------------------
RUN yum groupinstall -y "Development Tools"
RUN yum install -y gettext-devel openssl-devel perl-CPAN perl-devel zlib-devel wget
RUN wget https://github.com/git/git/archive/v2.7.0.tar.gz -O /tmp/git-2.7.0.tgz \
    && tar xzf /tmp/git-2.7.0.tgz --directory=/tmp \
    && cd /tmp/git-* \
    && make configure \
    && ./configure --prefix=/usr \
    && make install \
    && rm -rf /tmp/* \
    && yum clean all
RUN ssh-keygen -t rsa -N "" -f /root/.ssh/id_rsa
COPY configs/ssh_config /root/.ssh/config
RUN chmod 600 /root/.ssh/config

# ------------------------------------------------------------------------------------
# XMLStarlet 1.6.1
# ------------------------------------------------------------------------------------
RUN wget http://dl.fedoraproject.org/pub/epel/7/x86_64/x/xmlstarlet-1.6.1-1.el7.x86_64.rpm -O /tmp/xmlstarlet.rpm \ 
    && yum localinstall -y /tmp/xmlstarlet.rpm \
    && rm -rf /tmp/* \
    && yum clean all

# ------------------------------------------------------------------------------------
# Necessary dependencies to run GUI apps through simulation
# ------------------------------------------------------------------------------------
RUN yum install -y libXrender libXtst && yum clean all
RUN yum -y install xorg-x11-server-Xvfb \
    && yum clean all

# ------------------------------------------------------------------------------------
# PMD
# ------------------------------------------------------------------------------------
ENV PMD_VERSION=5.4.1 \
    PMD_HOME=/opt/pmd

RUN wget -q https://github.com/pmd/pmd/releases/download/pmd_releases%2F$PMD_VERSION/pmd-bin-$PMD_VERSION.zip -O /tmp/pmd.zip \
    && unzip -q /tmp/pmd.zip -d /opt \
    && ln -s /opt/pmd-bin-$PMD_VERSION $PMD_HOME \
    && rm -rf /tmp/*

# ------------------------------------------------------------------------------------
# Install cloc
# ------------------------------------------------------------------------------------
ENV CLOC_VERSION=1.66 \
    CLOC_HOME=/opt/cloc

RUN wget -q https://github.com/AlDanial/cloc/releases/download/v$CLOC_VERSION/cloc-$CLOC_VERSION.tar.gz -O /tmp/cloc.tar.gz \
    && tar zxf /tmp/cloc.tar.gz --directory=/opt \
    && ln -s /opt/cloc-$CLOC_VERSION $CLOC_HOME \
    && rm -rf /tmp/*

# ------------------------------------------------------------------------------------
# Necessary Python modules
# ------------------------------------------------------------------------------------
RUN yum install -y epel-release \
    && yum -y install python-devel python-pip python-setuptools \
    && yum clean all \
    && pip install --upgrade pip \
    && pip install grequests unidiff \
    && pip install pandas

# ------------------------------------------------------------------------------------
# Install SonarQube Runner
# ------------------------------------------------------------------------------------
ENV SONAR_RUNNER_VERSION=2.4 \
    SONAR_RUNNER_HOME=/opt/sonar-runner

ENV PATH=${PATH}:$SONAR_RUNNER_HOME/bin

RUN yum install -y which \
    && wget http://repo1.maven.org/maven2/org/codehaus/sonar/runner/sonar-runner-dist/2.4/sonar-runner-dist-2.4.zip -O /tmp/runner.zip \
    && unzip /tmp/runner.zip -d /opt \
    && ln -s /opt/sonar-runner-$SONAR_RUNNER_VERSION $SONAR_RUNNER_HOME \
    && rm -rf /tmp/*


RUN yum install -y firefox
# ------------------------------------------------------------------------------------
# Add template xml
# ------------------------------------------------------------------------------------
CMD [ "/scripts/run.sh" ]

COPY xml /xml
COPY scripts/ /scripts/
RUN chmod +x /scripts -R
COPY configs /configs