Spectata Execution
===

Execution module of the spectat application.
Depends on the Spectata-API and the Spectata-sonar modules to be run as well.
The container can be locally executed using the following command: 
```bash
docker run -it \
	-e RACK_ENV=development \
	-e GIT_BRANCH=<<branch name>> \
	-v <<absolute project path>>:/repo \
	--net=spectata \
	--link metrics-api \
	--link sonar-server \
	-it \
	--rm \
	'spectata-execution' \
	/bin/bash
```