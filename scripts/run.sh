#!/usr/bin/env bash

build_uuid=$(uuidgen)
project_root='/repo'
revision='HEAD'

# Script for running the tools that collect metrics

# Parse args
while getopts ":r:abcdefghijkl" o; do
    case "${o}" in
    a)
        echo "[DEBUG] $(basename $0) execute was triggered" >&2
        execute="execute"
        sonar_runner="sonar_runner"
        ;;
    b)
        echo "[DEBUG] $(basename $0) duration was triggered" >&2
        execute="execute"
        duration="duration"
        ;;
    c)
        echo "[DEBUG] $(basename $0) change was triggered" >&2
        change="change"
        ;;
    d)
        echo "[DEBUG] $(basename $0) duplicates was triggered" >&2
        duplicates="duplicates"
        ;;
    f)
        echo "[DEBUG] $(basename $0) pmd was triggered" >&2
        pmd="pmd"
        ;;
    g)
        echo "[DEBUG] $(basename $0) dead_code was triggered" >&2
        execute="execute"
        sonar_runner="sonar_runner"
        dead_code="dead_code"
        ;;
    h)
        echo "[DEBUG] $(basename $0) coverage was triggered" >&2
        execute="execute"
        coverage="coverage"
        ;;
    i)
        echo "[DEBUG] $(basename $0) coupling was triggered" >&2
        execute="execute"
        coupling="coupling"
        ;;
    k)
        echo "[DEBUG] $(basename $0) test class length was triggered" >&2
        execute="execute"
        sonar_runner="sonar_runner"
        class_length="class_length"
        ;;
    l)
        echo "[DEBUG] $(basename $0) mutation was triggered" >&2
        execute="execute"
        duration="duration"
        mutation="mutation"
        ;;
    r)
        echo "[DEBUG] $(basename $0) revision was triggered, Parameter: $OPTARG" >&2
        revision=$OPTARG
        ;;
    \?)
      echo "[ERROR] $(basename $0) Invalid option: -$OPTARG" >&2
      ;;
    :)
      echo "[ERROR] $(basename $0) Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
    esac
done

echo "[INFO] checking out project at: $revision"
cd $project_root
git reset --hard $revision
git checkout . -f
git clean -f -d
cd /

/scripts/run/00_* $project_root $build_uuid
/scripts/run/01_* $project_root $build_uuid
/scripts/run/02_* $project_root $build_uuid
/scripts/run/03_* $project_root $build_uuid
/scripts/run/04_* $project_root $build_uuid

if ! [ -z ${execute+empty} ]; then
    /scripts/run/run_tests.sh $project_root $build_uuid
fi
if ! [ -z ${sonar_runner+empty} ]; then
    /scripts/run/sonar_runner.sh $project_root $build_uuid
fi
if ! [ -z ${duplicates+empty} ]; then
    /scripts/run/duplicates.py $project_root $build_uuid
fi
if ! [ -z ${change+empty} ]; then
    /scripts/run/change.py $project_root $build_uuid
fi
if ! [ -z ${pmd+empty} ]; then
    /scripts/run/pmd.py $project_root $build_uuid
fi
if ! [ -z ${coverage+empty} ]; then
    /scripts/run/coverage_lines.py $project_root $build_uuid
    /scripts/run/coverage_total.py $project_root $build_uuid
fi
if ! [ -z ${dead_code+empty} ]; then
    /scripts/run/dead_code.py $project_root $build_uuid
fi
if ! [ -z ${coupling+empty} ]; then
    /scripts/run/coupling.py $project_root $build_uuid
fi
if ! [ -z ${duration+empty} ]; then
    /scripts/run/duration.py $project_root $build_uuid
fi
if ! [ -z ${class_length+empty} ]; then
    /scripts/run/class_length.py $project_root $build_uuid
fi
if ! [ -z ${mutation+empty} ]; then
    /scripts/run/mutation.py $project_root $build_uuid
fi

/scripts/run/dependant_changes.py $project_root $build_uuid
/scripts/run/99_* $project_root $build_uuid