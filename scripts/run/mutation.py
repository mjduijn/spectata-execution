#!/usr/bin/env python

import sys, os, subprocess, xml.etree.ElementTree, re
from utils import *

# Execute Pitest and send data items to spectata backend

repo_path = sys.argv[1]
build_id = sys.argv[2]

# Set target classes
os.chdir(repo_path)

production_files = set()
for root, dirnames, filenames in os.walk('src/main/java'):
    for filename in filenames:
        if filename.endswith('.java'):
            production_files.add(os.path.join(root.replace('src/main/java/', ''), filename))

test_files = []
for root, dirnames, filenames in os.walk('src/test/java'):
    for filename in filenames:
        test_files.append(os.path.join(root.replace('src/test/java/', ''), filename))

matching_production_files = set()
for test_file in test_files:
    matching_production_files.add(test_file.replace('Test', ''))
    matching_production_files.add(test_file)

mutation_files = production_files.intersection(matching_production_files)

print len(mutation_files)
for f in production_files:
    f = f.replace('/', '.').replace('.java', '')
    subprocess.check_output(['xmlstarlet',
                             'ed',
                             '--inplace',
                             '--subnode',
                             '/project/build/plugins/plugin/artifactId[text()=\'pitest-maven\']/../configuration/targetClasses',
                             '--type', 'elem',
                             '-n', 'param',
                             '-v', f,
                             repo_path + '/pom.xml'])


# Exclude test classes that have failing test cases, Include others based on surefire results
excluded_test_classes = []
included_test_classes = []
for root, dirnames, filenames in os.walk('target/surefire-reports'):
    for filename in filenames:
        if filename.startswith('TEST') and filename.endswith('xml'):
            x = xml.etree.ElementTree.parse(os.path.join(root, filename)).getroot()
            if int(x.get('errors')) + int(x.get('failures')) > 0:
                excluded_test_classes.append(x.get('name'))
            else:
                included_test_classes.append(x.get('name'))

print 'Included test classes:', included_test_classes


query = ['xmlstarlet', 'sel', '-t', '-c',
         """'/project/build/plugins/plugin/artifactId[text()="maven-surefire-plugin"]/..//excludes/child::*/text()'""",
         repo_path + '/pom.xml']

for c in subprocess.check_output(query).splitlines():
    pass

print 'Excluded test classes:', excluded_test_classes

for c in included_test_classes:
    subprocess.check_output(['xmlstarlet',
                             'ed',
                             '--inplace',
                             '--subnode',
                             '/project/build/plugins/plugin/artifactId[text()=\'pitest-maven\']/../configuration/targetTests',
                             '--type', 'elem',
                             '-n', 'param',
                             '-v', c,
                             repo_path + '/pom.xml'])

for c in excluded_test_classes:
    subprocess.check_output(['xmlstarlet',
                             'ed',
                             '--inplace',
                             '--subnode',
                             '/project/build/plugins/plugin/artifactId[text()=\'pitest-maven\']/../configuration/excludedClasses',
                             '--type', 'elem',
                             '-n', 'param',
                             '-v', c,
                             repo_path + '/pom.xml'])


#Run mutation tests
subprocess.call('mvn org.pitest:pitest-maven:mutationCoverage', shell=True)

# Post mutations to metrics
pattern = re.compile(r'(.*?)\.([^.]*)$')
mutation_ctr = 0



for mutation in xml.etree.ElementTree.parse('target/pit-reports/mutations.xml').getroot().findall('mutation'):
    refs = [{
        'path': 'src/main/java/' + mutation.find('mutatedClass').text.replace('.', '/') + '.java',
        'key': 'src/main/java/' + mutation.find('mutatedClass').text.replace('.', '/') + '.java',
        'lineNumbers': [mutation.find('lineNumber').text]
    }]

    if mutation.find('killingTest').text:
        result = pattern.match(mutation.find('killingTest').text.split('(')[0])
        testPath = 'src/test/java/' + result.group(1).replace('.', '/') + '.java'
        testKey = testPath + '/' + result.group(2)
        refs.append({
            'path': testPath,
            'key': testKey,
            'lineNumbers': []
        })

    data = {
        'buildId': build_id,
        'rule': mutation.find('mutator').text,
        'value': mutation.get('status'),
        'refs': refs
    }
    # print data
    postToMetricsAsyncCollect('/api/build/data', data)
    mutation_ctr += 1

waitForAsyncMetricRequests()
print "[INFO] Successfully posted", mutation_ctr, "mutations"