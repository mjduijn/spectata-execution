#!/usr/bin/env python

import sys, subprocess, os
from unidiff import PatchSet
from utils import *
import StringIO

repo_path = sys.argv[1]
build_id = sys.argv[2]
os.chdir(repo_path)

print "[INFO] Running Dependant Change detection"
# revisions = subprocess.check_output(["git", "log", "master", "--reverse", "--pretty=format:%H"]).splitlines()[1:]
revisions = subprocess.check_output(["git", "rev-list", "--branches", "master", "--no-merges"]).splitlines()[1:]

change_ctr = 0
for revision in revisions:
    changes = subprocess.check_output(["git", "show", revision, "--unified=0"])
    changes = unicode(changes, errors='replace')

    patches = PatchSet(StringIO.StringIO(changes))
    for patch in patches:
        if patch.path.endswith(".java"):
            line_numbers_removed = []
            line_numbers_added = []
            if patch.is_added_file: change = "ADDED"
            elif patch.is_removed_file: change = "REMOVED"
            elif patch.is_modified_file: change = "MODIFIED"

            for hunk in patch:
                line_numbers_removed.extend(range(hunk.source_start, hunk.source_start + hunk.source_length))
                line_numbers_added.extend(range(hunk.target_start, hunk.target_start + hunk.target_length))

            postToMetricsAsyncCollect("/api/build/dependantchange", {
                "buildId": build_id,
                "revision": revision,
                "path": patch.path,
                "change": change,
                "lineNumbersRemoved": line_numbers_removed,
                "lineNumbersAdded": line_numbers_added
            })

            change_ctr += 1
            # if change_ctr % 1000 == 0:
            #     print "[DEBUG] sent", change_ctr, "changes"

waitForAsyncMetricRequests()
print '[INFO] Succesfully sent', change_ctr, 'changes'