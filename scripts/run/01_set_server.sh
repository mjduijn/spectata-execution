#!/usr/bin/env bash

if [ "$RACK_ENV" = 'development' ]; then

    metrics_api='spectata-api:8080'
    sonar_server='sonar-server:9000'
fi

echo "$metrics_api" > /metrics_api
echo "$sonar_server" > /sonar_server
sed -i "s/my-sonar-server/http:\/\/$sonar_server/" $M2_HOME/conf/settings.xml

echo "[INFO] Using Cassandra API at: $metrics_api"
echo "[INFO] Using Sonar Server at: $sonar_server"