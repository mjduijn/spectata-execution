#!/usr/bin/env bash

cp /configs/sonar-project.properties $1/sonar-project.properties

sonar_server=$(cat /sonar_server)

jar=$(find target -name *.jar | head)

sed -i "s/sonar.projectKey=.*/sonar.projectKey=$2/g" $1/sonar-project.properties
sed -i "s/sonar.projectName=.*/sonar.projectName=$2/g" $1/sonar-project.properties
sed -i "s/sonar.host.url=.*/sonar.host.url=http:\/\/$(cat /sonar_server)/g" $1/sonar-project.properties
sed -i "s/sonar.java.libraries=.*/sonar.java.libraries=$jar/g" $1/sonar-project.properties

cd $1
sonar-runner

echo $(cat .sonar/report-task.txt | grep ceTaskId | cut -d'=' -f2) > /sonar_runner_task_id