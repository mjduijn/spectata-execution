#!/usr/bin/env python

import sys, json, urllib2, urllib, subprocess, os, csv, shutil, pandas as pd
import xml.etree.ElementTree as ET
from utils import *

# Execute PMD and send data items to spectata backend

repo_path = sys.argv[1]
test_path = repo_path + "/src/test"
output_path = repo_path + '/target/pmd.csv'
build_id = sys.argv[2]
pmd_path = os.environ["PMD_HOME"] + "/bin/run.sh"
os.chdir(repo_path)

libs = [f for f in os.listdir('/binaries') if f.startswith('pmd')]
for lib in libs:
    shutil.copyfile('/binaries/' + lib, os.environ['PMD_HOME'] + '/lib/' + lib)

print "[INFO] Running PMD analysis"
try:
    with open(output_path, 'w') as f:
        subprocess.call([pmd_path, 
            "pmd", 
            "-rulesets", "/xml/pmd.xml",
            "-language", "java", 
            "-failOnViolation", "false", 
            "-format", "xml",
            "-dir", test_path], stdout=f)

except subprocess.CalledProcessError as e:
    print "[ERROR] PMD analysis failed with error code: %d" % e.returncode
    print e.cmd
    print e.output
    sys.exit(e.returncode)

print "[DEBUG] PMD analysis successfull, sending issues..."


ctr = 0
for file in ET.parse(output_path).getroot().findall('file'):
    path = file.get('name')
    path = path[path[1:].index('/') + 2:] #Skip /repo part of path
    for violation in file.findall('violation'):
        value = 1
        if violation.get('ruleset') == 'JUnit':
            line_numbers = range(int(violation.get('beginline')), int(violation.get('endline')))
        else:
            line_numbers = []
            try:
                value = float(violation.text)
            except Exception as e:
                print "Could not parse " + violation.text
                print e
                continue

        key = path
        if violation.get('method'):
            key = key + "/" + violation.get('method')

        ctr += 1
        data = {
            'buildId': build_id,
            'rule': violation.get('rule'),
            'path': path,
            'key': key,
            'lineNumbers': line_numbers,
            'value': value
        }

        postToMetricsAsyncCollect('/api/build/issue', data)

print "[INFO] Successfully sent", ctr, "issues"
waitForAsyncMetricRequests()