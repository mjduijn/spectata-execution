#!/usr/bin/env python

import sys
from utils import *

repo_path = sys.argv[1]
build_id = sys.argv[2]

waitForSonarMavenTask()

def retrieve_store_coverage(coverage_type):
    metrics = getSonarMetric(coverage_type, getProjectKey())
    test_cov_counter = 0

    for m in metrics:
        data = {
            'buildId': build_id,
            'value': m['value'],
            'rule': coverage_type,
            'refs': [{
                'key': m['path'],
                'path': m['path'],
                'lineNumbers': []
            }]
        }
        postToMetricsAsyncCollect('/api/build/data', data)
        test_cov_counter += 1

    print '[INFO] Succesfully sent', test_cov_counter, coverage_type

retrieve_store_coverage('coverage')
retrieve_store_coverage('line_coverage')
retrieve_store_coverage('branch_coverage')

waitForAsyncMetricRequests()