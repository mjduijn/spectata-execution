#!/usr/bin/env python

import sys, json, urllib2, urllib, subprocess, os
from unidiff import PatchSet
import codecs
import unicodedata
import StringIO
from utils import *

repo_path = sys.argv[1]
build_id = sys.argv[2]
os.chdir(repo_path)

print "[INFO] Running Change detection on", repo_path

# Get current git revisions
revisions = subprocess.check_output(["git", "rev-list", "HEAD", "--reverse"]).splitlines()

# Find the latest parent build and get its git revision
url = getMetricUrl() + '/api/build'
params = urllib.urlencode({'sonarprojectkey': getProjectKey()})

req = urllib2.Request(url, params)
response = urllib2.urlopen(req)
builds = json.load(response)

parent = {}
highest_parent_revision_idx = 0
parent_build_id = build_id

for build in builds:
    if(build["buildId"] != build_id) and build["scmRevisions"] is not None:
        idx = 0
        while(idx < len(build["scmRevisions"])
                and idx < len(revisions)
                and build["scmRevisions"][idx] == revisions[idx]):
            if idx > highest_parent_revision_idx:
                highest_parent_revision_idx = idx
                parent_build_id = build["buildId"]
            idx += 1

print '[DEBUG] highest_parent_revision_idx:', highest_parent_revision_idx


# Calculate difference between this build and parent build
# Post changes to storage
url = getBaseMetricUrl() + '/api/build/change'
diff_path = 'target/diff'

changes = subprocess.check_output(["git", "diff", revisions[highest_parent_revision_idx], "--unified=0", "HEAD"])
changes = unicode(changes, errors='replace')

patches = PatchSet(StringIO.StringIO(changes))

for patch in patches:
    hunks = []
    line_numbers_removed = []
    line_numbers_added = []
    for hunk in patch:
        hunks.append({
            "added": hunk.added,
            "removed": hunk.removed,
            "sourceStart": hunk.source_start,
            "sourceLength": hunk.source_length,
            "targetStart": hunk.target_start,
            "targetLength": hunk.target_length
        })
        line_numbers_removed.extend(range(hunk.source_start, hunk.source_start + hunk.source_length))
        line_numbers_added.extend(range(hunk.target_start, hunk.target_start + hunk.target_length))

    change = ""
    if patch.is_added_file:
        change = "ADDED"
    if patch.is_removed_file:
        change = "REMOVED"
    if patch.is_modified_file:
        change = "MODIFIED"
    data = {
        "buildId": build_id,
        "change": change,
        "path": patch.path,
        "key": getProjectKey() + ":" + patch.path,
        "lineNumbersRemoved": line_numbers_removed,
        "lineNumbersAdded": line_numbers_added
    }

    req = urllib2.Request(url)
    req.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(req, json.dumps(data))
    if(response.getcode() != 200): 
        print "[ERROR] Could not post change to", url
        print json.dumps(data)
        exit(1)

print "[INFO] Successfully posted", len(patches), "changes to", url


#Set revisions in build
data = {'buildId': build_id,
        'scmRevisions': revisions,
        'parentBuildId': parent_build_id}

url = getBaseMetricUrl() + '/api/build'

req = urllib2.Request(url)  
req.add_header('Content-Type', 'application/json')
response = urllib2.urlopen(req, json.dumps(data))


if response.getcode() != 200:
    print "[ERROR] Sending revisions gave error to:", url
    exit(1)

print "[INFO] Successfully posted updated build to:", url
