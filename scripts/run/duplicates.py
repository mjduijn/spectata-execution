#!/usr/bin/env python

import sys, json, urllib2, urllib, subprocess, os, csv, datetime
import xml.etree.ElementTree as ET
from utils import *

#Execute CPD and send clones to backend

repo_path = sys.argv[1]
test_path = repo_path + "/src/test"
output_path = repo_path + '/target/cpd.xml'
build_id = sys.argv[2]
pmd_path = os.environ["PMD_HOME"] + "/bin/run.sh"
os.chdir(repo_path)


print "[DEBUG] Running CPD analysis", datetime.datetime.utcnow().isoformat()
try:
    with open(output_path, 'w') as f:
        subprocess.call([pmd_path, 
            "cpd", 
            "--minimum-tokens", "100",
            "--language", "java",
            # "--skip-lexical-errors",
            "--failOnViolation", "false", 
            "--format", "xml",
            "--ignore-literals",
            # "--ignore-identifiers",
            "--files", test_path], stdout=f)

except subprocess.CalledProcessError as e:
    print "CPD analysis failed with error code: %d" % e.returncode
    print e.cmd
    print e.output
    sys.exit(e.returncode)

print "[DEBUG] Posting duplicate result to server", datetime.datetime.utcnow().isoformat()
# Post duplicates to storage
duplicate_count = 0
for duplicate in ET.parse(output_path).getroot().findall('duplication'):
    refs = []
    for r in duplicate.findall('file'):
        path = r.get('path')
        path = path[path[1:].index('/') + 2:]
        refs.append({
            'path': path,
            'key': path,
            'lineNumbers': range(int(r.get('line')), int(r.get('line')) + int(duplicate.get('lines'))),
        })

    data = {
        'buildId': build_id,
        'rule': 'Duplicate',
        'value': duplicate.get('lines'),
        'refs': refs,
    }
    postToMetricsAsyncCollect("/api/build/data", data)
    duplicate_count += 1

waitForAsyncMetricRequests()
print "[INFO] Successfully posted", duplicate_count, "duplicates"
