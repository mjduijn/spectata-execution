#!/usr/bin/env bash

if [ -z "$RACK_ENV" ]; then 
    echo "[ERROR] Environment variable RACK_ENV not set or empty"
    exit 1
fi


echo "$2" > /build_id