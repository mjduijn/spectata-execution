#!/usr/bin/env python

import sys
from utils import *
from functools import partial
import xml.etree.ElementTree as ET

#Send test duration to backend

repo_path = sys.argv[1]
build_id = sys.argv[2]

waitForSonarMavenTask()

# From all test classes get each test case
# Example request: http://localhost:9000/api/tests/list?testFileKey=org.apache.commons:commons-lang3:src/test/java/org/apache/commons/lang3/AnnotationUtilsTest.java
def retrieve_tests(test_classes, url_params):
    for test_class in test_classes:
        getSonarData('/api/tests/list', {'testFileKey': test_class['key']},
                     partial(handle_sonar_response, post_test_cases_to_metrics, 'tests'))

test_case_ctr = 0
#Post each test case to metrics API
def post_test_cases_to_metrics(test_cases, url_params):
    global test_case_ctr
    for test_case in test_cases:
        test_case_ctr += 1
        postToMetricsAsyncCollect('/api/build/issue', {
            'buildId': build_id,
            'rule': 'DURATION',
            'path': test_case['fileName'],
            'key': test_case['fileName'] + "/" + test_case['name'],
            'lineNumbers': [],
            'value': test_case['durationInMs']
        })

# Get all test classes
# example request: http://localhost:9000/api/components/tree?baseComponentKey=org.apache.commons:commons-lang3&qualifiers=UTS
getSonarData("/api/components/tree", {'baseComponentKey': getProjectKey(), 'qualifiers': 'UTS'},
             cb=partial(handle_sonar_response, retrieve_tests, 'components'))

waitForAsyncSonarRequests()
waitForAsyncMetricRequests()

print '[INFO] Successfully posted', test_case_ctr, 'durations'
