#!/usr/bin/env bash

group_id=$(awk '{print tolower($0)}' "$1/pom.xml" | xmlstarlet sel -t -c "/project/groupid/text()")
artifact_id=$(awk '{print tolower($0)}' "$1/pom.xml" | xmlstarlet sel -t -c "/project/artifactid/text()")

echo "$group_id:$artifact_id" > /projectkey
echo "[INFO] $(basename $0) Project key: $(cat /projectkey)"

url="$(cat /metrics_api)/api/raw/sonarprojectkey/$2"