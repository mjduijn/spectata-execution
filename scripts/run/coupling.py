#!/usr/bin/env python

from utils import *
from functools import partial

repo_path = sys.argv[1]
test_path = repo_path + '/src/test'
build_id = sys.argv[2]

waitForSonarMavenTask()

tests_sent_ctr = 0
# (3) Store covered lines in metrics
def store_tests(test_cases, url_params):
    global tests_sent_ctr
    for test_case in test_cases:
        data = {
            'buildId': build_id,
            'testKey': test_case['fileName'] + "/" + test_case['name'],
            'productionKey': url_params['sourceFilePath'],
            'line': url_params['sourceFileLineNumber']
        }
        postToMetricsAsyncCollect('/api/build/testcoupling', data)
        tests_sent_ctr += 1
        if tests_sent_ctr % 100000 == 0:
            print '[DEBUG] Succesfully sent', tests_sent_ctr, 'test couplings'


# (1) Get all covered production lines of code
metrics = getSonarMetric('overall_coverage_line_hits_data', getProjectKey())


# (2) Get per covered production line of code which test cases cover it
for m in metrics:
    lines_covered = [int(x[0:x.index("=")]) for x in m['value'].split(';') if x.endswith('1')]
    for l in lines_covered:
        params = {'sourceFileKey': getProjectKey() + ':' + m['path'], 'sourceFileLineNumber': l, 'sourceFilePath': m['path']}
        getSonarData('/api/tests/list', params=params, cb=partial(handle_sonar_response, store_tests, 'tests'))

waitForAsyncSonarRequests()
waitForAsyncMetricRequests()

print '[INFO] Succesfully sent', tests_sent_ctr, 'test couplings'