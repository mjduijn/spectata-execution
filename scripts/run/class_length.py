#!/usr/bin/env python

import sys
from utils import *

repo_path = sys.argv[1]
test_path = repo_path + '/src/test'
build_id = sys.argv[2]

waitForSonarRunnerTask()
print '[INFO] Retrieving test length information'

def get_metric_value(component):
    if len(component['measures']) > 0:
        return component['measures'][0]['value']

ncloc_ctr = 0
#Store ncloc
def cb_ncloc(components, url_params):
    global ncloc_ctr
    for component in components:
        value = get_metric_value(component) if get_metric_value(component) else 0
        postToMetricsAsyncCollect('/api/build/data', {
            'buildId': build_id,
            'value': value,
            'rule': 'NCLOC',
            'refs': [{
                'key': component['path'],
                'path': component['path'],
                'lineNumbers': []
            }]
        })
    ncloc_ctr += len(components)

#Retrieve ncloc
#Example request: http://localhost:9000/api/measures/component_tree?baseComponentKey=org.apache.commons:commons-lang3&metricKeys=ncloc
getSonarData('/api/measures/component_tree', {'baseComponentKey': build_id, 'metricKeys': 'ncloc'},
             cb=partial(handle_sonar_response, cb_ncloc, 'components'))


coverage_line_hits_ctr = 0
def cb_coverage_line_hits(components, url_params):
    global coverage_line_hits_ctr
    for component in components:
        value = len(get_metric_value(component).split(';')) if get_metric_value(component) else 0
        postToMetricsAsyncCollect('/api/build/data', {
            'buildId': build_id,
            'value': value,
            'rule': 'COVERAGE_LINE_HITS_COUNT',
            'refs': [{
                'key': component['path'],
                'path': component['path'],
                'lineNumbers': []
            }]
        })
    coverage_line_hits_ctr += len(components)


#Retrieve overall_coverage_line_hits_data
#Example request: http://localhost:9000/api/measures/component_tree?baseComponentKey=org.apache.commons:commons-lang3&metricKeys=ncloc
getSonarData('/api/measures/component_tree', {'baseComponentKey': build_id, 'metricKeys': 'overall_coverage_line_hits_data'},
             cb=partial(handle_sonar_response, cb_coverage_line_hits, 'components'))


lines_ctr = 0
def cb_lines(components, url_params):
    global lines_ctr
    for component in components:
        value = get_metric_value(component) if get_metric_value(component) else 0
        postToMetricsAsyncCollect('/api/build/data', {
            'buildId': build_id,
            'value': value,
            'rule': 'LINES',
            'refs': [{
                'key': component['path'],
                'path': component['path'],
                'lineNumbers': []
            }]
        })
    lines_ctr += len(components)


#Retrieve lines per file
#Example request: http://localhost:9000/api/measures/component_tree?baseComponentKey=org.apache.commons:commons-lang3&metricKeys=lines
getSonarData('/api/measures/component_tree', {'baseComponentKey': build_id, 'metricKeys': 'lines'},
             cb=partial(handle_sonar_response, cb_lines, 'components'))


waitForAsyncSonarRequests()
waitForAsyncMetricRequests()

print "[INFO] Successfully posted", ncloc_ctr, "nclocs"
print "[INFO] Successfully posted", coverage_line_hits_ctr, "coverage_line_hits_ctr info"
print "[INFO] Successfully posted", lines_ctr, "lines_ctr info"
