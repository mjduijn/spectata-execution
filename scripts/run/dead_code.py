#!/usr/bin/env python

import sys, json, urllib2, urllib, subprocess, os, csv, shutil, inspect
from unidiff import PatchSet
from utils import *

# Post the dead code in src/test
# Any line that is not covered in any file in /src/test
# => All files in /src/test should help with testing
#    If any are not used during testing than they are (or should be) never used

repo_path = sys.argv[1]
test_path = repo_path + '/src/test'
build_id = sys.argv[2]

waitForSonarRunnerTask()

metrics = getSonarMetric('overall_coverage_line_hits_data', build_id)
metrics = [m for m in metrics if m['path'].startswith('src/test')]

ctr = 0
for m in metrics:
    lines_not_covered = [int(x[:x.index("=")]) for x in m['value'].split(';') if x.endswith('0')]

    for l in lines_not_covered:
        data = {
            'buildId': build_id,
            'value': 1,
            'rule': 'DEAD_CODE',
            'refs': [{
                'key': m['path'],
                'path': m['path'],
                'lineNumbers': [l]
            }]
        }
        postToMetricsAsyncCollect('/api/build/data', data)
        ctr += 1

waitForAsyncMetricRequests()
print "[INFO] Successfully posted", ctr, "lines of dead code"