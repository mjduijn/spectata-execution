#!/usr/bin/env python

# Post the lines covered and not covered

import sys
from utils import *

repo_path = sys.argv[1]
build_id = sys.argv[2]
lines_covered_count = 0
lines_not_covered_count = 0

waitForSonarMavenTask()
metrics = getSonarMetric('overall_coverage_line_hits_data', getProjectKey())

for m in metrics:
    lines_not_covered = [int(x[0:x.index("=")]) for x in m['value'].split(';') if x.endswith('0')]
    lines_covered = [int(x[0:x.index("=")]) for x in m['value'].split(';') if x.endswith('1')]
    for l in lines_not_covered:
        data = {
            'buildId': build_id,
            'value': 0,
            'rule': 'LINE_NOT_COVERED',
            'refs': [{
                'key': m['path'],
                'path': m['path'],
                'lineNumbers': [l]
            }]
        }
        postToMetricsAsyncCollect('/api/build/data', data)
        lines_not_covered_count += 1

    for l in lines_covered:
        data = {
            'buildId': build_id,
            'value': 0,
            'rule': 'LINE_COVERED',
            'refs': [{
                'key': m['path'],
                'path': m['path'],
                'lineNumbers': [l]
            }]
        }
        postToMetricsAsyncCollect('/api/build/data', data)
        lines_covered_count += 1

waitForAsyncMetricRequests()

print "[INFO] Successfully posted", lines_not_covered_count, "lines not covered"
print "[INFO] Successfully posted", lines_covered_count, "lines covered"