#!/usr/bin/env bash

echo "[INFO] $(basename $0) editing pom at $1/pom.xml"

cd $1
git checkout pom.xml

cp $1/pom.xml $1/pom.old.xml

xmlstarlet fo $1/pom.xml > /tmp/pom1.xml
sed -i '0,/project.*/{s/project.*/project\>/}' /tmp/pom1.xml

xmlstarlet ed --inplace --delete "/project/developers" /tmp/pom1.xml
xmlstarlet ed --inplace --delete "/project/contributors" /tmp/pom1.xml
xmlstarlet ed --inplace --delete "/project/licenses" /tmp/pom1.xml
xmlstarlet ed --inplace --delete "/project/scm" /tmp/pom1.xml
xmlstarlet ed --inplace --delete "/project/organization" /tmp/pom1.xml

dependencies=$(xmlstarlet sel -t -c "/project/dependencies" /tmp/pom1.xml)
if [ -z "$dependencies" ]; then
    xmlstarlet ed --inplace --subnode "/project" --type elem -n "dependencies" -v "" /tmp/pom1.xml
fi

plugins_path="/project/build/plugins"

plugins=$(xmlstarlet sel -t -c "$plugins_path" /tmp/pom1.xml)
if [ -z "$plugins" ]; then
    plugins_path="/project/build/pluginManagement/plugins"
fi
plugins=$(xmlstarlet sel -t -c "$plugins_path" /tmp/pom1.xml)
if [ -z "$plugins" ]; then
    echo "No plugins found in POM"
    exit 1
fi

#Force JUnit 4
xmlstarlet ed --inplace --delete "/project/dependencies/dependency/groupId[text()='junit']/.." /tmp/pom1.xml
xmlstarlet ed --inplace --subnode "/project/dependencies" --type elem -n "dependency" -v "$(sed -r 's/\s+//g' /xml/junit.xml)" /tmp/pom1.xml

#Jacoco listener dependency
xmlstarlet ed --inplace --delete "/project/dependencies/artifactId[text()='sonar-jacoco-listeners']/.." /tmp/pom1.xml
xmlstarlet ed --inplace --subnode "/project/dependencies" --type elem -n "dependency" -v "$(sed -r 's/\s+//g' /xml/sonarqube_jacoco.xml)" /tmp/pom1.xml

#Setup surefire
executions=$(xmlstarlet sel -t -c "$plugins_path/plugin/artifactId[text()='maven-surefire-plugin']/../executions/*[id/text() != 'integration-tests']" /tmp/pom1.xml)
configuration=$(xmlstarlet sel -t -c "$plugins_path/plugin/artifactId[text()='maven-surefire-plugin']/../configuration/*" /tmp/pom1.xml)

xmlstarlet ed --inplace --delete "$plugins_path/plugin/artifactId[text()='maven-surefire-plugin']/.." /tmp/pom1.xml
xmlstarlet ed --inplace --subnode "$plugins_path" --type elem -n "plugin" -v "$(sed -r 's/\s+//g' /xml/surefire.xml)" /tmp/pom1.xml
sed -i 's/&lt;/\</g' /tmp/pom1.xml
sed -i 's/&gt;/\>/g' /tmp/pom1.xml

#Make sure configuration exists
if [ -n "$configuration" ]; then
    xmlstarlet ed --inplace --subnode "$plugins_path/plugin/artifactId[text()='maven-surefire-plugin']/.." --type elem -n "configuration" -v "$configuration" /tmp/pom1.xml
fi

#Make sure configuration/properties exists
properties=$(xmlstarlet sel -t -c "$plugins_path/plugin/artifactId[text()='maven-surefire-plugin']/../configuration/properties" /tmp/pom1.xml)
if [ -z "$properties" ]; then
    xmlstarlet ed --inplace --subnode "$plugins_path/plugin/artifactId[text()='maven-surefire-plugin']/../configuration" --type elem -n "properties" -v "" /tmp/pom1.xml
fi
xmlstarlet ed --inplace --subnode "$plugins_path/plugin/artifactId[text()='maven-surefire-plugin']/../configuration/properties" --type elem -n "property" -v "$(sed -r 's/\s+//g' /xml/surefire_listener.xml)" /tmp/pom1.xml

if [ -n "$executions" ]; then
    xmlstarlet ed --inplace --subnode "$plugins_path/plugin/artifactId[text()='maven-surefire-plugin']/.." --type elem -n "executions" -v "$executions" /tmp/pom1.xml
fi

#sonar.host.url
xmlstarlet ed --inplace --delete "/project/properties/sonar.host.url" /tmp/pom1.xml
xmlstarlet ed --inplace --subnode "/project/properties" --type elem -n "sonar.host.url" -v "http://$(cat /sonar_server)" /tmp/pom1.xml
#sonar.jacoco.reportPath
xmlstarlet ed --inplace --delete "/project/properties/sonar.jacoco.reportPath" /tmp/pom1.xml
xmlstarlet ed --inplace --subnode "/project/properties" --type elem -n "sonar.jacoco.reportPath" -v "target/jacoco.exec" /tmp/pom1.xml
#sonar.cpd.skip=true
xmlstarlet ed --inplace --delete "/project/properties/sonar.cpd.skip" /tmp/pom1.xml
xmlstarlet ed --inplace --subnode "/project/properties" --type elem -n "sonar.cpd.skip" -v "true" /tmp/pom1.xml
#sonar.scm.provider
xmlstarlet ed --inplace --delete "/project/properties/sonar.scm.provider" /tmp/pom1.xml
xmlstarlet ed --inplace --subnode "/project/properties" --type elem -n "sonar.scm.provider" -v "git" /tmp/pom1.xml

#Remove Failsafe
xmlstarlet ed --inplace --delete "$plugins_path/plugin/artifactId[text()='pitest-maven']/.." /tmp/pom1.xml

#Add Pitest
xmlstarlet ed --inplace --delete "$plugins_path/plugin/artifactId[text()='pitest-maven']/.." /tmp/pom1.xml
xmlstarlet ed --inplace --subnode "$plugins_path" --type elem -n "plugin" -v "$(sed -r 's/\s+//g' /xml/pitest.xml)" /tmp/pom1.xml

sed -i 's/&lt;/\</g' /tmp/pom1.xml
sed -i 's/&gt;/\>/g' /tmp/pom1.xml

#Remove any comments
xmlstarlet ed --inplace --delete '//comment()' /tmp/pom1.xml

xmlstarlet fo /tmp/pom1.xml > /tmp/pom2.xml
xmlstarlet val /tmp/pom2.xml > /dev/null

cp /tmp/pom2.xml $1/pom.xml