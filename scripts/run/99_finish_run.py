#!/usr/bin/env python

# ------------------------------------------------------------------------------------
# Post new entry in run database
# ------------------------------------------------------------------------------------

import requests

from utils import *

repo_path = sys.argv[1]
build_id = sys.argv[2]

data = {
    'buildId': build_id,
    'status': 'EXECUTION_DONE',
}

try:
    r = requests.post(getMetricUrl() + "/api/build", json=data)
except requests.exceptions.RequestException as e:
    print e
    sys.exit(1)