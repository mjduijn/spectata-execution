#!/usr/bin/env python

# ------------------------------------------------------------------------------------
# Post new entry in run database
# ------------------------------------------------------------------------------------

from utils import *
import requests
import pytz
from datetime import datetime

repo_path = sys.argv[1]
build_id = sys.argv[2]

u = datetime.utcnow()
u = u.replace(tzinfo=pytz.utc)
u = u.replace(microsecond=0)

data = {
    'buildId': build_id,
    'startTs': u.isoformat(),
    'projectKey': getProjectKey(),
    'status': 'EXECUTING',
}

try:
    r = requests.post(getMetricUrl() + "/api/build", json=data)
except requests.exceptions.RequestException as e:
    print e
    sys.exit(1)