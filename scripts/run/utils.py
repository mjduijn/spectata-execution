#!/usr/bin/env python

import os, urllib2, json, sys, urllib, grequests, threading
from time import sleep
from urlparse import urlparse, parse_qs
from functools import partial
from requests.exceptions import RequestException

def getKeyFromPath(path):
    return getProjectKey() + ":" + path[path[1:].index('/') + 2:]

def getPathFromSonarKey(key):
    return key.split(':')[2:]


def __readMetricUrl():
    url = ''
    with open('/metrics_api') as f:
        url = 'http://' + f.read().strip()
    return url
__metric_url = __readMetricUrl()

def getMetricUrl():
    return __metric_url

def getBaseMetricUrl():
    return getMetricUrl()

def __readProjectKey():
    key = ''
    with open('/projectkey') as f:
        key = f.read().strip()
    return key
__project_key = __readProjectKey()

def getProjectKey(): 
    return __project_key

def __readSonarUrl():
    url = ''
    with open('/sonar_server') as f:
        url = f.read().strip()
    return 'http://' + url

__sonar_url = __readSonarUrl()
def getSonarUrl():
    return __sonar_url


def postToMetrics(endpoint, data):
    url = getMetricUrl() + endpoint
    
    req = urllib2.Request(url)
    req.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(req, json.dumps(data))
    if(response.getcode() != 200): 
        print "[ERROR] Could not post data to", url
        print json.dumps(data)
        sys.exit(1)

# __metricPool = grequests.Pool(20)
__metricPool = grequests.Pool(20)
__out = 0
def postToMetricsAsync(endpoint, data, callback=None):
    if callback is not None:
        req = grequests.post(getMetricUrl() + endpoint, json=data, callback=callback)
    else:
        req = grequests.post(getMetricUrl() + endpoint, json=data)
    grequests.send(req, pool=__metricPool, stream=True)
    global __out
    __out += 1
    #print "[DEBUG] number of outstanding requests:", __out



def __cb(r, **kwargs):
    r.raise_for_status()
    global __out
    __out -= 1
    #print "[DEBUG] number of outstanding requests:", __out


__collected = {}
__lock = threading.Lock()


def postToMetricsAsyncCollect(endpoint, data):
    __lock.acquire()
    sending = []
    if not __collected.has_key(endpoint):
        __collected[endpoint] = []
    __collected[endpoint].append(data)

    if len(__collected[endpoint]) >= 1000:
        sending = __collected[endpoint][0:1000]
        __collected[endpoint] = __collected[endpoint][1000:]
    __lock.release()

    if len(sending) > 0:
        postToMetricsAsync(endpoint, sending, callback=__cb)

def waitForAsyncMetricRequests():
    __metricPool.join() # First make sure nobody else wants to send
    # __lock.acquire()
    for endpoint, c in __collected.iteritems():
        if len(c) > 0:
            postToMetricsAsync(endpoint, c)
    # __lock.release()
    __metricPool.join()

def getSonarMetric(metric, base_component_key):
    url = getSonarUrl() + '/api/measures/component_tree'
    components = []
    page = 1
    while True:
        params = urllib.urlencode({'metricKeys': metric, 'baseComponentKey': base_component_key, 'qualifiers': 'FIL', 'p': page})
        req = urllib2.Request(url, params)
        print req
        response = urllib2.urlopen(req)
        component_tree = json.load(response) 
        #TODO incorporate error handling
        if len(component_tree['components']) == 0:
            return components
        for component in component_tree['components']:
            if len(component['measures']) > 0:
                components.append({'path': component['path'], 'value': component['measures'][0]['value']})
        page += 1

def __waitForSonarTask(task_id):
    if task_id is None or task_id == "":
        print "[DEBUG] No sonar maven task available, not waiting"
    else:
        while True:
            url = getSonarUrl() + '/api/ce/task'
            params = urllib.urlencode({'id': task_id})
            req = urllib2.Request(url, params)
            response = urllib2.urlopen(req)
            task = json.load(response)['task']
            if task['status'] == 'SUCCESS':
                break
            sleep(0.5)
            print "[DEBUG] Waiting on Sonar task %s to finish" % task_id

def waitForSonarMavenTask():
    with open('/sonar_maven_task_id', 'r') as f:
        __waitForSonarTask(f.read().strip())

def waitForSonarRunnerTask():
    with open('/sonar_runner_task_id', 'r') as f:
        __waitForSonarTask(f.read().strip())


# __sonarPool = grequests.Pool(150)
__sonarPool = grequests.Pool()

def handle_sonar_response(data_func, key, r, **kwargs):
    try:
        r.raise_for_status()
    except RequestException as e:
        print '[ERROR]', e

    content = r.json()
    url = urlparse(r.url)
    url_params = {}
    for name, value in parse_qs(url.query).iteritems():
        url_params[name] = (",").join(value)
    max_page_size = int(content['paging']['pageSize'])
    page = int(content['paging']['pageIndex'])
    page_size = len(content[key])
    if page_size == max_page_size:
        url_params['p'] = page + 1
        request = grequests.get(getSonarUrl() + url.path, params=url_params, callback=partial(handle_sonar_response, data_func, key))
        grequests.send(request, pool=__sonarPool)

    # Handle data
    data_func(content[key], url_params)


def getSonarData(endpoint, params, cb):
    request = grequests.get(getSonarUrl() + endpoint, params=params, callback=cb)
    grequests.send(request, pool=__sonarPool)


def waitForAsyncSonarRequests():
    __sonarPool.join()