#!/usr/bin/env bash

# ------------------------------------------------------------------------------------
# Run Xvfb (virtual display)
# ------------------------------------------------------------------------------------
#echo starting headless X.org server, display :0
#Xvfb :0 -screen 0 1024x768x16 -ac +extension GLX +render -noreset &
#export DISPLAY=:0

# ------------------------------------------------------------------------------------
# Run tests
# ------------------------------------------------------------------------------------
cd $1

mvn clean org.jacoco:jacoco-maven-plugin:0.7.7.201606060606:prepare-agent test -Dmaven.test.failure.ignore=true -Danimal.sniffer.skip

jar=$(find target -name *.jar | head)
xmlstarlet ed --inplace --subnode "/project/properties" --type elem -n "sonar.java.libraries" -v "$jar" /tmp/pom1.xml

export SONAR_RUNNER_OPTS="-Xmx6g -XX:MaxPermSize=2g -XX:ReservedCodeCacheSize=128m -XX:+UseConcMarkSweepGC -XX:-UseGCOverheadLimit"
export MAVEN_OPTS="-Xmx6g -XX:MaxPermSize=2g -XX:ReservedCodeCacheSize=128m -XX:+UseConcMarkSweepGC -XX:-UseGCOverheadLimit"

mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.0.2:sonar

echo $(cat target/sonar/report-task.txt | grep ceTaskId | cut -d'=' -f2) > /sonar_maven_task_id

